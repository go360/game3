# README #

This README helps to compile, test and run the code 

The application is using a centralized ActiveMQ instance for remote communication between two players.

### Special feature
Two Players can play this came on different network (WAN / LAN), Players do not have any constraint to play within the same network (LAN), I hosting ActiveMQ service on my AWS server to enable joy for players sitting at their home on different locations 

**Note:** Event messaging is hosted on a free server so it is a bit slow, please be patient and wait for your events :) 

## Project ##
Game of Three

## ActiveMQ ##
* Protocol: TCP
* Host: [139.9.89.142]
* Port: 61616

## Project Nature ##
* A simple Maven Java Project 
* Event based (ActiveMQ)
* Anemic domain model (DDD) 
* Configurable (MessageBroker / GamePlay) 


### What is this repository for? ###
* Coding challenge

### How do I get set up? ###

* Install Java 11
* Install Maven (Latest)
* Lombok setup please visit [https://projectlombok.org/setup/overview]

### Build, Test and Run ###

* mvn package
* java -jar game3.jar

### Setup Local ActiveMQ ###
* Go to root directory of the project /game3
* docker-compose up -d
* Go to Application.java and update the CONNECTION_URL tcp://[hostname]:61616
* mvn package
* java -jar game3.jar


## How to play

When user starts the application, following inputs are needed,

**Please Game Zone Name:**

*User to provide a unique Game Zone Name and press Enter. Name of Game Zone to be same for both players. The game allows many users to play together in couples by defining their GameZones*

**Please enter your name:**

*User to provide player name and press Enter*

** Do you want to Autoplay (Y/N):**

*User to specify whether Manual or Automatic mode, Automatic mode will automatically send the number to the other Player, where Manual mode will just suggest you the calculated number and you have to press 'Enter' to send.*

**Please enter a first whole number (not less than 2):**

*User to enter a starting number. User starting application first will be prompted to enter the number first.
The user who enters the first Number will become a Lead Player (first player) and the other player will automatically just join the lead player in the game zone*

### Code Coverage ###
* Coverage: 51.9%
* Covered Instructions: 426
* Missed Instructions: 395
* Total Instructions: 821

### Who do I talk to? ###

* mysialkot@hotmail.com