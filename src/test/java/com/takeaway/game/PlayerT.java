package com.takeaway.game;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Objects;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.takeaway.game.jms.Consumer;
import com.takeaway.game.jms.Producer;
import com.takeaway.game.util.GameInterruptException;
import com.takeaway.game.util.GameMessage;

import lombok.SneakyThrows;

@RunWith(MockitoJUnitRunner.class)
public class PlayerT {

	Player player = null;
	String playerName = "PlayerA";

	@Before
	public void init() {
		player = new Player(playerName, true);
	}

	@Test
	public void canInitializeGameHappyT() {
		Producer.notifyAsStarter(playerName);
		String otherPlayer = Consumer.ifAnyoneStarted();
		assertTrue(Objects.equals(playerName, otherPlayer));
	}

	@Test(expected = GameInterruptException.class)
	public void evaluateNumber1T() {
		player.evaluateNumber(1);
	}

	@Test(expected = GameInterruptException.class)
	public void evaluateNumber0T() {
		player.evaluateNumber(0);
	}

	@Test
	public void findNextNumberT() {
		assertThat(13, is(player.findNextNumber(38)));
		assertThat(12, is(player.findNextNumber(37)));
		assertThat(12, is(player.findNextNumber(36)));
	}

	@Test
	@SneakyThrows
	public void onMessageT() {

		GameMessage messageA = new GameMessage("PlayerA", 35, "ID");
		player.sendMessage(messageA);
		Thread.sleep(5000);
		assertThat(35, is(player.getCurrentMessage().getNumber()));

	}

	@Test
	@SneakyThrows
	public void gameMessageToString() {
		GameMessage messageA = new GameMessage("PlayerA", 1, "playerID");
		ObjectMapper om = new ObjectMapper();
		assertThat("{\"from\":\"PlayerA\",\"number\":1,\"notification\":null,\"playerID\":\"playerID\"}", is(om.writeValueAsString(messageA)));

	}

}
