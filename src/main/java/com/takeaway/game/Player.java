package com.takeaway.game;

import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.takeaway.game.jms.Consumer;
import com.takeaway.game.jms.Producer;
import com.takeaway.game.util.GameInterruptException;
import com.takeaway.game.util.GameMessage;

import lombok.Getter;
import lombok.SneakyThrows;

/**
 * 
 * @author Sikandar Ali Awan
 * @since 30-08-2020
 *
 */

public class Player extends Thread implements MessageListener {

	private static final ObjectMapper OM = new ObjectMapper();
	private String player;
	private String playerID = UUID.randomUUID().toString();
	private Consumer consumer;
	private Boolean autoPlay = true;
	@Getter
	private volatile GameMessage currentMessage = null;

	public Player(String player, Boolean autoPlay) {
		consumer = new Consumer(this);
		this.player = player;
		this.autoPlay = autoPlay;
		consumer.start();
	}

	@SuppressWarnings("resource")
	@SneakyThrows
	/**
	 * Run function of the Player Thread It contains the core logic of game play
	 */
	public void run() {

		Integer number = getInitialNumer();

		try {
			while (true) {

				while (currentMessage == null || Objects.equals(currentMessage.getPlayerID(), playerID)) {
					sleep(1000);
				}

				System.out.println(currentMessage.getFrom() + " Sent -> " + currentMessage.getNumber());

				number = findNextNumber(currentMessage.getNumber());

				if (!autoPlay) {
					System.out.println("Suggested number '" + number + "', -> Press enter to send");
					new Scanner(System.in).nextLine();
				}

				System.out.println("You sent -> " + number);
				evaluateNumber(number);
				sendMessage(new GameMessage(player, number, playerID));
			}
		} catch (GameInterruptException e) {
			sendMessage(e.getGameMessage());
		}

		System.out.println(currentMessage.getNotification() + " -> Thank You :)");
		System.exit(0);

	}

	/*
	 * {-1, 0, +1} The logic to find nearest multiple of three
	 */
	public Integer findNextNumber(Integer number) {
		return (int) Math.round(((double) number) / 3);
	}

	/*
	 * Evaluate the number to decide winner and game status
	 */
	public void evaluateNumber(Integer number) {
		GameMessage gameMessage = new GameMessage(player, number, playerID);
		if (number < 2) {
			gameMessage.setNotification("Winner:" + player);
			throw new GameInterruptException(gameMessage);
		}
	}

	/*
	 * Method to push notification to the other Player
	 */
	public void sendMessage(GameMessage message) {
		this.currentMessage = message;
		Producer.notify(message);
	}

	/**
	 * The function takes the first whole number to start the game and become a Lead
	 * Player, But if the game has been already started by another player (Lead
	 * Player) in the same "Game Zone" then you can just join them (automatically)
	 */
	@SuppressWarnings("resource")
	public Integer getInitialNumer() {
		Integer number = 0;
		String otherPlayer = Consumer.ifAnyoneStarted();
		if (otherPlayer != null) {
			System.out.println("Your are joining Player: " + otherPlayer);
		} else {
			Producer.notifyAsStarter(player);
			do {
				System.out.print("Please enter a first whole number (not less than 2): ");
				try {
					number = Integer.parseInt(new Scanner(System.in).nextLine());
					sendMessage(new GameMessage(player, number, playerID));
				} catch (Exception e) {
					System.err.println("Your are not plyaing seriously :( , Please enter correct number");
				}
			} while (number < 2);

		}
		return number;
	}

	/*
	 * Message Listener based on the "GameZone"
	 */
	@Override
	public void onMessage(Message message) {
		try {
			final TextMessage textMessage = (TextMessage) message;
			currentMessage = OM.readValue(textMessage.getText(), GameMessage.class);
			if (currentMessage.getNotification() != null) {
				System.out.println(currentMessage.getNotification());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
