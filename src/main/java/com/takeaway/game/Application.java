package com.takeaway.game;

import java.util.Scanner;

/**
 * 
 * @author Sikandar Ali Awan
 * @since 30-08-2020
 *
 */

public class Application {

	public static String CONNECTION_URL = "tcp://139.9.89.142:61616";
	public static String EXCHANGE = "exchange";
	public static String GAME_LEAD = "gameLead";

	/**
	 * Main Method of the Game, this takes the input of GameZone name, Player Name,
	 * and AutoPlay (Y/N) GameZone name should be same for two players who wants to
	 * play this game, this game allows many users to play together in couples by
	 * defining their GameZones
	 * 
	 * AutoPlay: If 'Y' means your system will automatically send the number to the
	 * other Player, else system will just suggest you the calculated number and you
	 * have to press 'Enter' to send.
	 */

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please Game Zone Name: ");
		EXCHANGE = GAME_LEAD = scanner.nextLine().toLowerCase();
		System.out.println("Please enter your name: ");
		String player = scanner.nextLine();
		System.out.println("Do you want to Autoplay (Y/N): ");
		String autoPlay = scanner.nextLine();
		new Player(player, autoPlay.equalsIgnoreCase("y")).start();

	}

}
