package com.takeaway.game.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Sikandar Ali Awan
 * @since 30-08-2020
 *
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameMessage {

	private String from;
	private Integer number;
	private String notification;
	private String playerID;

	public GameMessage(String from, Integer number, String playerID) {
		super();
		this.from = from;
		this.number = number;
		this.playerID = playerID;
	}

}
