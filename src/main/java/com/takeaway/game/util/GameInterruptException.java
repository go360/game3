package com.takeaway.game.util;

public class GameInterruptException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7114232808493280422L;
	private GameMessage gameMessage;

	public GameInterruptException(String message) {
		super(message);
	}

	public GameInterruptException(GameMessage gameMessage) {
		this.gameMessage = gameMessage;
	}

	public GameMessage getGameMessage() {
		return gameMessage;
	}

}
