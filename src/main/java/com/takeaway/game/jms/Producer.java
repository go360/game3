package com.takeaway.game.jms;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.takeaway.game.Application;
import com.takeaway.game.util.GameMessage;

/**
 * 
 * @author Sikandar Ali Awan
 * @since 30-08-2020
 *
 */

public class Producer {

	private static final ObjectMapper OM = new ObjectMapper();

	public static void notify(final GameMessage message) {
		notify(message, Application.EXCHANGE);
	}

	public static void notifyAsStarter(String player) {
		send(player, Application.GAME_LEAD);
	}

	private static void notify(final GameMessage message, final String topic) {

		try {
			// Create a ConnectionFactory
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Application.CONNECTION_URL);

			// Create a Connection
			Connection connection = connectionFactory.createConnection();
			connection.start();

			// Create a Session
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// Create the destination (Topic or Queue)
			Destination destination = session.createTopic(topic);

			// Create a MessageProducer from the Session to the Topic or Queue
			MessageProducer producer = session.createProducer(destination);
			producer.setDeliveryMode(DeliveryMode.PERSISTENT);

			// Create a messages
			TextMessage textMessage = session.createTextMessage(OM.writeValueAsString(message));

			// Tell the producer to send the message
			producer.send(textMessage);

			// Clean up
			session.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private static void send(final String message, final String queue) {

		try {
			// Create a ConnectionFactory
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Application.CONNECTION_URL);

			// Create a Connection
			Connection connection = connectionFactory.createConnection();
			connection.start();

			// Create a Session
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// Create the destination (Topic or Queue)
			Destination destination = session.createQueue(queue);

			// Create a MessageProducer from the Session to the Topic or Queue
			MessageProducer producer = session.createProducer(destination);
			producer.setDeliveryMode(DeliveryMode.PERSISTENT);

			// Create a messages
			TextMessage textMessage = session.createTextMessage(message);

			// Tell the producer to send the message
			producer.send(textMessage);

			// Clean up
			session.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}