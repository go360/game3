package com.takeaway.game.jms;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.takeaway.game.Application;

/**
 * 
 * @author Sikandar Ali Awan
 * @since 30-08-2020
 *
 */

public class Consumer extends Thread implements ExceptionListener {

	private MessageListener messageListener;

	public Consumer(MessageListener messageListener) {
		this.messageListener = messageListener;

	}

	public void run() {
		try {
			// Create a ConnectionFactory
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Application.CONNECTION_URL);

			// Create a Connection
			Connection connection = connectionFactory.createConnection();
			connection.start();

			connection.setExceptionListener(this);

			// Create a Session
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

			// Create the destination (Topic or Queue)
			Destination destination = session.createTopic(Application.EXCHANGE);

			// Create a MessageConsumer from the Session to the Topic or Queue
			MessageConsumer consumer = session.createConsumer(destination);
			while (true) {
				Message message = consumer.receive(1000);
				if (message != null)
					messageListener.onMessage(message);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String ifAnyoneStarted() {
		String player = null;
		try {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Application.CONNECTION_URL);
			Connection connection = connectionFactory.createConnection();
			connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination destination = session.createQueue(Application.GAME_LEAD);
			MessageConsumer consumer = session.createConsumer(destination);
			Message message = consumer.receive(1000);
			if (message != null)
				player = ((TextMessage) message).getText();
			consumer.close();
			session.close();
			connection.close();
		} catch (JMSException e) {
			e.printStackTrace();

		}
		return player;
	}

	public synchronized void onException(JMSException ex) {
		System.out.println("OMG -> Sikandar -> JMS Exception occured.  Shutting down client Oooooo...");
	}

}
